package br.com.agenda.controllers;

import br.com.agenda.models.Contato;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContatoController {

    @GetMapping("/{nome}")
    public Contato create(@PathVariable String nome){
        Contato contato = new Contato();

        contato.setNome(nome);
        return contato;
    }
}
